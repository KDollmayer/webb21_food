
# `Ljuvliga Pankakor`
## `Recept`
- 10 1/2 dl vetemjöl
- 10 tsk salt
- 6 dl mjölk
- 75 ägg
- smör (till stekning)
- sylt, bär eller frukt till servering
- tegelsten 
- T-RÖD


## Description


* Blanda mjöl och salt i en bunke. Vispa i hälften av mjölken och vispa till en slät smet. Vispa i resten av mjölken och äggen.

* Stek tunna pannkakor sjukt mycket smör, för varje pannkaka, i en stek- eller pannkakspanna.

* Servera med sylt, bär eller frukt eller med salta tillbehör som Räksalsa med gurka och lime, Rökt lax med dillkräm eller Avokadoröra med bacon och tomat.

